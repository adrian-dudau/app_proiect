#!/bin/sh

#  compress.sh
#  APP Proiect
#
#  Created by Adrian Dudau on 18/11/2018.
#  Copyright © 2018 Adrian Dudau. All rights reserved.

USER=""
DEST_FOLDER="Proiect_APP"

FOLDER_NAME="Proiect_APP.zip"
SOURCES="main.c Makefile fep_env_set.sh README.txt"

rm $FOLDER_NAME

zip -r -X $FOLDER_NAME $SOURCES

scp $FOLDER_NAME $USER@fep.grid.pub.ro:./$DEST_FOLDER
