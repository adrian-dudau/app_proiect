EXE=crypt_executable
CC=gcc
CCFLAGS=-Wall -g

FILE_SIZE=256
BT=m

PARAM_1=${FILE_SIZE}${BT}_random_data.in

#############################################

${EXE}: main.c
	${CC} ${CCFLAGS} main.c -o ${EXE}

run: ${EXE}
	time ./${EXE} ${PARAM_1}

bin:
	time dd if=/dev/urandom of=${PARAM_1} bs=1${BT} count=${FILE_SIZE}

clean:
	rm -rf ${EXE}* test.*
