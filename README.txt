# Create the random binary input file to encrypt
$ make bin

# setup environment for fep and run app once with solaris profiler
$ sh fep_env_set.sh

# Steps:
$ make bin
$ make run

# or on Fep
$ make bin
$ sh fep_env_set.sh


