//
//  main.c
//  APP Proiect
//
//  Created by Adrian Dudau on 18/11/2018.
//  Copyright © 2018 Adrian Dudau. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//#define PRINT_DEBUG // uncomment this for I/O print text
//#define PRINT_CORRECTION // uncomment this for I/O print correctness

#define DATA_CHUNK_SIZE 8 // 8 * 8 bytes read == 64bit (32 + 32 bits chunks)
#define MY_KEYS "1234567887654321" // 16 characters | 4 * 4 bytes | 4 * 32 bits

/* from wikipedia. search for TEA */
void tea_encrypt (uint32_t v[2], uint32_t k[4]) {
    uint32_t v0=v[0], v1=v[1], sum=0, i;           /* set up */
    uint32_t delta=0x9E3779B9;                     /* a key schedule constant */
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
    for (i=0; i<32; i++) {                         /* basic cycle start */
        sum += delta;
        v0 += ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
        v1 += ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
    }                                              /* end cycle */
    v[0]=v0; v[1]=v1;
}

/* from wikipedia. search for TEA */
void tea_decrypt (uint32_t v[2], uint32_t k[4]) {
    uint32_t v0=v[0], v1=v[1], sum=0xC6EF3720, i;  /* set up; sum is 32*delta */
    uint32_t delta=0x9E3779B9;                     /* a key schedule constant */
    uint32_t k0=k[0], k1=k[1], k2=k[2], k3=k[3];   /* cache key */
    for (i=0; i<32; i++) {                         /* basic cycle start */
        v1 -= ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
        v0 -= ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
        sum -= delta;
    }                                              /* end cycle */
    v[0]=v0; v[1]=v1;
}

int main(int argc, const char * argv[]) {
    
    if (argc != 2) {
        printf("Usage: %s <input_file>", argv[0]);
        exit(-1);
    }
    
    FILE* in = NULL;
    in = fopen(argv[1], "rb");
    
    if (in == NULL) {
        perror("Unable to open file\n");
        exit(-2);
    }
    
    uint32_t data_read_len = 0;
    
    char *original_string = (char*)calloc(1, (DATA_CHUNK_SIZE + 1)); // + 1 for newline
#ifdef PRINT_DEBUG
    char *final_string = (char*)calloc(1, (DATA_CHUNK_SIZE + 1)); // + 1 for newline
#endif
    uint32_t *crypt_block = (uint32_t*)malloc(2 * sizeof(uint32_t));
    uint32_t *crypt_keys = (uint32_t*)malloc(4 * sizeof(uint32_t));
    
    memcpy(crypt_keys, MY_KEYS, 4 * sizeof(uint32_t));
    
    data_read_len = (uint32_t)fread(original_string, sizeof(char), DATA_CHUNK_SIZE, in);
    original_string[data_read_len] = 0;
    
    while (data_read_len > 0) {
#ifdef PRINT_DEBUG
        printf("BeforeE:%s\n", original_string);
#endif
        // copy string from file to memory for encrypt
        memcpy(crypt_block, original_string, DATA_CHUNK_SIZE);
        // encrypt
        tea_encrypt(crypt_block, crypt_keys);
#ifdef PRINT_DEBUG
        memcpy(final_string, crypt_block, DATA_CHUNK_SIZE);
        printf("AfterE:%s\n", final_string);
#endif
        // decrypt
        tea_decrypt(crypt_block, crypt_keys);
        
#if defined(PRINT_DEBUG) || defined (PRINT_CORRECTION)
//        original_string[0] = 'x';
        if(memcmp(original_string, crypt_block, DATA_CHUNK_SIZE) == 0)
            printf("Crypted Correctly\n");
        else
            printf("Crypted Incorrectly\n");
#endif
#ifdef PRINT_DEBUG
        // copy for null terminated print
        memcpy(final_string, crypt_block, DATA_CHUNK_SIZE);
        printf("AfterD:%s\n", final_string);
        printf("\n");
#endif
        
        data_read_len = (uint32_t)fread(original_string, sizeof(char), DATA_CHUNK_SIZE, in);
        original_string[data_read_len] = 0;
#ifdef PRINT_DEBUG
        final_string[data_read_len] = 0;
#endif
    }
    
    return 0;
}
